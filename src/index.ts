const title = 'Hello TypeScript' as string;

document.getElementById('content')!.innerHTML = `${title}`;

const btnStart = document.getElementById("btnStart");

// Run myfunc every second

function btnListener() {
    const date = new Date();
    const referenceTime = date.setHours(date.getHours() + 2);
    localStorage.setItem("referenceTime", JSON.stringify(referenceTime));

    countdown();
    // getCard();

}

function countdown() {
    let myfunc = setInterval(function () {
        const date = JSON.parse(localStorage.getItem('referenceTime'));
        btnStart.removeEventListener("click", btnListener);
        let now = new Date();
        let timeleft = +date - +now;

        // Calculating the days, hours, minutes and seconds left
        let days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
        let hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((timeleft % (1000 * 60)) / 1000);

        // Result is output to the specific element
        document.getElementById("days").innerHTML = days + "d "
        document.getElementById("hours").innerHTML = hours + "h "
        document.getElementById("mins").innerHTML = minutes + "m "
        document.getElementById("secs").innerHTML = seconds + "s "

        // Display the message when countdown is over
        if (timeleft < 0) {
            clearInterval(myfunc);
            document.getElementById("days").innerHTML = ""
            document.getElementById("hours").innerHTML = ""
            document.getElementById("mins").innerHTML = ""
            document.getElementById("secs").innerHTML = ""
            document.getElementById("end").innerHTML = "TIME UP!!";
            btnStart.addEventListener("click", btnListener);
        }
        // console.log(timeleft);

    }, 1000);
}

let test = localStorage.getItem("referenceTime");
if (test) {
    countdown()
}

// function getCard() {
    const mainCategory = ["people", "planets", "species", "starships", "vehicles"];
    const mainCount = [82, 60, 37, 36, 39];

    const randomNumber = Math.floor(Math.random() * mainCategory.length);

    let randomCategory = mainCategory[randomNumber];
    let max = mainCount[randomNumber];

    const randomId = Math.floor(Math.random() * max + 1)

    const url = `https://swapi.dev/api/${randomCategory}/${randomId}`

    fetch(url)
        .then(response => response.json())
        .then(data => console.table(data));

        
// localStorage.setItem(data)
// }

// to do -display card name , 
// store card once, 
// recall list of cards ,
// associate cards to assets images,
// cote html
// CSS bootstrap?
// quiz while they wait:))??